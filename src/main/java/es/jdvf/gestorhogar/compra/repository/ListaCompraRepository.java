package es.jdvf.gestorhogar.compra.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import es.jdvf.gestorhogar.compra.entity.ListaCompraEntity;

public interface ListaCompraRepository extends MongoRepository<ListaCompraEntity, String> {

}
