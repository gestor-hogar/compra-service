package es.jdvf.gestorhogar.compra.service;

import java.util.List;

import es.jdvf.gestorhogar.compra.dto.ListaCompra;

public interface CompraService {

	List<ListaCompra> avisos(String email);

}
