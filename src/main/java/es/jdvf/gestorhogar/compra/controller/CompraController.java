package es.jdvf.gestorhogar.compra.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.jdvf.gestorhogar.compra.dto.ListaCompra;
import es.jdvf.gestorhogar.compra.exception.CompraException;
import es.jdvf.gestorhogar.compra.service.CompraService;
import io.swagger.annotations.ApiOperation;

/**
 * Controller gestión de lista de la compra
 *
 */
@RestController
@RequestMapping("/api/compra")
public class CompraController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CompraController.class);

	@Autowired
	private CompraService service;

	/**
	 * Devuelve avisos del usuario de cambios en las listas de la compra
	 * 
	 * @param email
	 * @return
	 */
	@ApiOperation(value = "Devuelve avisos del usuario de cambios en las listas de la compra.", response = ListaCompra.class)
	@RequestMapping(value = "/{email}/avisos", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	ResponseEntity<List<ListaCompra>> avisos(@PathVariable("email") String email) {
		LOGGER.info("avisos - begin - email: {}", email);
		List<ListaCompra> result = service.avisos(email);
		LOGGER.info("avisos - result: {}", result);
		return ResponseEntity.ok(result);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleTodoNotFound(CompraException ex) {
		LOGGER.error("Handling error with message: {}", ex.getMessage());
	}
}
