package es.jdvf.gestorhogar.compra.util;

import es.jdvf.gestorhogar.compra.dto.ListaCompra;
import es.jdvf.gestorhogar.compra.entity.ListaCompraEntity;

public class CompraUtil {

	/**
	 * Devuelve objeto DTO familia a partir de la entidad
	 * @param entity
	 * @return
	 */
	public static ListaCompra convertToDTO(ListaCompraEntity entity) {
		return ListaCompra.builder().id(entity.getId())
				.nombre(entity.getNombre())
				.build();
	}
	
	/**
	 * Devuelve objeto entidad familia a partir del DTO
	 * @param dto
	 * @return
	 */
	public static ListaCompraEntity convertToEntity(ListaCompra dto) {
		return ListaCompraEntity.builder().id(dto.getId())
				.nombre(dto.getNombre())
				.build();
	}
}
