package es.jdvf.gestorhogar.compra.exception;

/**
 * This exception is thrown when the requested familia entity is not found.
 */
public class CompraException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompraException(String id) {
        super(String.format("No familia entity found with id: <%s>", id));
    }
}
