package es.jdvf.gestorhogar.compra.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ListaCompra {
	// Identificador de familia
	@Id
	private String id;
	// Nombre
	@NotEmpty
	private String nombre;
	// Lista de productos
	private List<Producto> productos;
}
