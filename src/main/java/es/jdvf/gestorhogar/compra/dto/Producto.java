package es.jdvf.gestorhogar.compra.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Producto {
	// Identificador
	@Id
	private String id;
	// Tipo
	@NotEmpty
	private String tipo;
	// detalle
	@NotEmpty
	private String detalle;
	
}
