package es.jdvf.gestorhogar.compra.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import es.jdvf.gestorhogar.compra.dto.Producto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(collection = "listacompra")
public class ListaCompraEntity {
	// id
	private String id;
	// Nombre
	private String nombre;
	// Lista de productos
	private List<Producto> productos;
}